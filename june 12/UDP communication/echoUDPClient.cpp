#include "PracticalSocket.h"      // For UDPSocket and SocketException
#include <iostream>               // For cout and cerr
#include <cstdlib>                // For atoi()
#include <cstring>
using namespace std;

const int ECHOMAX = 255;          // Longest string to echo

/*arg[1] is server ip address
 arg[2] is message to send to server
 arg [3] is server port
*/
int main(int argc, char *argv[]) {
  

  string servAddress = argv[1];             // First arg: server address
  char* echoString = argv[2];               // Second arg: string to echo
  int echoStringLen = strlen(echoString);   // Length of string to echo
  
  //create socket
  UDPSocket sock;
  
  //resolving port
  unsigned short echoServPort = Socket::resolveService(
    (argc == 4) ? argv[3] : "echo", "udp");
  
  // Send the string to the server
  sock.sendTo(echoString, echoStringLen, servAddress, echoServPort);
  
  
  // Receive a response
    char echoBuffer[ECHOMAX + 1];       // Buffer for echoed string + \0
    int respStringLen;
	respStringLen=sock.recvFrom(echoBuffer, ECHOMAX, servAddress, echoServPort);
	
	echoBuffer[respStringLen] = '\0';             // Terminate the string!
    cout << "Received: " << echoBuffer << endl;   // Print the echoed arg
	
	return 0;
}