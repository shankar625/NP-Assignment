#include "PracticalSocket.h" // For UDPSocket and SocketException
#include <iostream>          // For cout and cerr
#include <cstdlib>           // For atoi()
#include <string.h>
#include <unistd.h>			// for fork()

const int ECHOMAX = 255;     // Longest string to echo

/*
argv[1] is server port
*/
int main(int argc, char *argv[]) {

  unsigned short echoServPort = atoi(argv[1]);     // First arg:  local port

  
    UDPSocket sock(echoServPort);                
  
    char echoBuffer[ECHOMAX];         // Buffer for echo string
    int recvMsgSize;                  // Size of received message
    string sourceAddress;             // Address of datagram source
    unsigned short sourcePort;        // Port of datagram source
    for (;;) {  // Run forever
      // Block until receive message from a client
	  
	  if(fork() == 0) {
		  /* If we're the child*/
		recvMsgSize = sock.recvFrom(echoBuffer, ECHOMAX, sourceAddress, sourcePort);
  
		cout << "Received packet from " << sourceAddress << ":" 
			<< sourcePort << endl;
  
		sock.sendTo(echoBuffer, recvMsgSize, sourceAddress, sourcePort);
	  }
	  else{
		  /* Otherwise, we must be the parent and our work for this client is finished. */
		  
		  //clean buffer
		  memset(echoBuffer, 0, sizeof(echoBuffer));
	  }
    }
  
  

  return 0;
}